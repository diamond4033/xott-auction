﻿namespace XoTT_Auction.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Lot_StartPricaAndStep_FromInt_ToDecimal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Lot", "StartPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Lot", "Step", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Lot", "Step", c => c.Int(nullable: false));
            AlterColumn("dbo.Lot", "StartPrice", c => c.Int(nullable: false));
        }
    }
}
