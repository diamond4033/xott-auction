﻿namespace XoTT_Auction.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Removed_NavProps_in_Users : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Lot", "Buyer_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Lot", "Owner_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Lot", new[] { "Buyer_Id" });
            DropIndex("dbo.Lot", new[] { "Owner_Id" });
            CreateIndex("dbo.LotPicture", "LotId");
            AddForeignKey("dbo.LotPicture", "LotId", "dbo.Lot", "Id");
            DropColumn("dbo.Lot", "Buyer_Id");
            DropColumn("dbo.Lot", "Owner_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Lot", "Owner_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.Lot", "Buyer_Id", c => c.String(maxLength: 128));
            DropForeignKey("dbo.LotPicture", "LotId", "dbo.Lot");
            DropIndex("dbo.LotPicture", new[] { "LotId" });
            CreateIndex("dbo.Lot", "Owner_Id");
            CreateIndex("dbo.Lot", "Buyer_Id");
            AddForeignKey("dbo.Lot", "Owner_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Lot", "Buyer_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
