﻿namespace XoTT_Auction.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Lot_UserId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Bid", "LotId", "dbo.Lot");
            DropForeignKey("dbo.Lot", "CategoryId", "dbo.Category");
            DropForeignKey("dbo.LotPicture", "LotId", "dbo.Lot");
            DropForeignKey("dbo.LotPicture", "PictureId", "dbo.Picture");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            AddColumn("dbo.Lot", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Lot", "UserId");
            AddForeignKey("dbo.Lot", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Bid", "LotId", "dbo.Lot", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Lot", "CategoryId", "dbo.Category", "Id", cascadeDelete: true);
            AddForeignKey("dbo.LotPicture", "LotId", "dbo.Lot", "Id", cascadeDelete: true);
            AddForeignKey("dbo.LotPicture", "PictureId", "dbo.Picture", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.LotPicture", "PictureId", "dbo.Picture");
            DropForeignKey("dbo.LotPicture", "LotId", "dbo.Lot");
            DropForeignKey("dbo.Lot", "CategoryId", "dbo.Category");
            DropForeignKey("dbo.Bid", "LotId", "dbo.Lot");
            DropForeignKey("dbo.Lot", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Lot", new[] { "UserId" });
            DropColumn("dbo.Lot", "UserId");
            AddForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles", "Id");
            AddForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.LotPicture", "PictureId", "dbo.Picture", "Id");
            AddForeignKey("dbo.LotPicture", "LotId", "dbo.Lot", "Id");
            AddForeignKey("dbo.Lot", "CategoryId", "dbo.Category", "Id");
            AddForeignKey("dbo.Bid", "LotId", "dbo.Lot", "Id");
        }
    }
}
