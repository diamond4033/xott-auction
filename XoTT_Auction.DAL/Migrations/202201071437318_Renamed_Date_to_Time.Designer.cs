﻿// <auto-generated />
namespace XoTT_Auction.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class Renamed_Date_to_Time : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Renamed_Date_to_Time));
        
        string IMigrationMetadata.Id
        {
            get { return "202201071437318_Renamed_Date_to_Time"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
