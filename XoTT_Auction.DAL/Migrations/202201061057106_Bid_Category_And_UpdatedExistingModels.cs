﻿namespace XoTT_Auction.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Bid_Category_And_UpdatedExistingModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bid",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BidAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Timestamp = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LotId = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Lot", t => t.LotId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.LotId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 125),
                        Description = c.String(maxLength: 300),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Lot", "ActualPrice", c => c.Int(nullable: false));
            AddColumn("dbo.Lot", "CategoryId", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "PicUrl", c => c.String(maxLength: 260));
            AlterColumn("dbo.Lot", "Title", c => c.String(nullable: false, maxLength: 150));
            AlterColumn("dbo.Lot", "StartingDate", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Lot", "EndingDate", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.AspNetUsers", "FirstName", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.AspNetUsers", "LastName", c => c.String(nullable: false, maxLength: 40));
            CreateIndex("dbo.Lot", "CategoryId");
            AddForeignKey("dbo.Lot", "CategoryId", "dbo.Category", "Id");
            DropColumn("dbo.Lot", "ActualAmount");
            DropColumn("dbo.Lot", "ModifiedOn");
            DropColumn("dbo.AspNetRoles", "Discriminator");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetRoles", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.Lot", "ModifiedOn", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Lot", "ActualAmount", c => c.Int(nullable: false));
            DropForeignKey("dbo.Bid", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Bid", "LotId", "dbo.Lot");
            DropForeignKey("dbo.Lot", "CategoryId", "dbo.Category");
            DropIndex("dbo.Lot", new[] { "CategoryId" });
            DropIndex("dbo.Bid", new[] { "UserId" });
            DropIndex("dbo.Bid", new[] { "LotId" });
            AlterColumn("dbo.AspNetUsers", "LastName", c => c.String());
            AlterColumn("dbo.AspNetUsers", "FirstName", c => c.String());
            AlterColumn("dbo.Lot", "EndingDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Lot", "StartingDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Lot", "Title", c => c.String());
            DropColumn("dbo.AspNetUsers", "PicUrl");
            DropColumn("dbo.Lot", "CategoryId");
            DropColumn("dbo.Lot", "ActualPrice");
            DropTable("dbo.Category");
            DropTable("dbo.Bid");
        }
    }
}
