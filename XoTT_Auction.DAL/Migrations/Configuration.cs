﻿namespace XoTT_Auction.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<XoTT_Auction.DAL.Context.AuctionContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(XoTT_Auction.DAL.Context.AuctionContext context)
        {
            //This method will be called after migrating to the latest version. Use it if you needed, XoTT
        }
    }
}
