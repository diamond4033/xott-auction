﻿namespace XoTT_Auction.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Lot_AddedStep_ActualPrice_To_StartPrice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Lot", "StartPrice", c => c.Int(nullable: false));
            AddColumn("dbo.Lot", "Step", c => c.Int(nullable: false));
            DropColumn("dbo.Lot", "ActualPrice");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Lot", "ActualPrice", c => c.Int(nullable: false));
            DropColumn("dbo.Lot", "Step");
            DropColumn("dbo.Lot", "StartPrice");
        }
    }
}
