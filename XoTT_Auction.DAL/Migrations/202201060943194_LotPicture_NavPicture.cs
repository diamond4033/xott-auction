﻿namespace XoTT_Auction.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class LotPicture_NavPicture : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.LotPicture", "PictureId");
            AddForeignKey("dbo.LotPicture", "PictureId", "dbo.Picture", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LotPicture", "PictureId", "dbo.Picture");
            DropIndex("dbo.LotPicture", new[] { "PictureId" });
        }
    }
}
