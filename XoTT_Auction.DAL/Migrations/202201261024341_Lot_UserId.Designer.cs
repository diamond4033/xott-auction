﻿// <auto-generated />
namespace XoTT_Auction.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class Lot_UserId : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Lot_UserId));
        
        string IMigrationMetadata.Id
        {
            get { return "202201261024341_Lot_UserId"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
