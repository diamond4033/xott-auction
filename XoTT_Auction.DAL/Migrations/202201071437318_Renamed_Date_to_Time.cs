﻿namespace XoTT_Auction.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Renamed_Date_to_Time : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Lot", "StartingTime", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Lot", "EndingTime", c => c.DateTime(precision: 7, storeType: "datetime2"));
            DropColumn("dbo.Lot", "StartingDate");
            DropColumn("dbo.Lot", "EndingDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Lot", "EndingDate", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Lot", "StartingDate", c => c.DateTime(precision: 7, storeType: "datetime2"));
            DropColumn("dbo.Lot", "EndingTime");
            DropColumn("dbo.Lot", "StartingTime");
        }
    }
}
