﻿using XoTT_Auction.DAL.Context;
using XoTT_Auction.DAL.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace XoTT_Auction.DAL.Managers
{
    /// <summary>
    /// class to manage users
    /// </summary>
    public class AuctionUserManager : UserManager<User>
    {
        /// <summary>
        /// user manager constructor
        /// </summary>
        /// <param name="store"></param>
        public AuctionUserManager(UserStore<User> store)
            : base(store)
        { }
        /// <summary>
        /// method to create new user manager for Identity
        /// </summary>
        /// <param name="options"></param>
        /// <param name="owinContext"></param>
        /// <returns>new user manager</returns>
        public static AuctionUserManager Create(IdentityFactoryOptions<AuctionUserManager> options,
            IOwinContext owinContext)
        {

            AuctionContext auctionContext = owinContext.Get<AuctionContext>();
            UserStore<User> userStore = new UserStore<User>(auctionContext);
            AuctionUserManager manager = new AuctionUserManager(userStore);

            manager.UserValidator = new UserValidator<User>(manager)
            {
                AllowOnlyAlphanumericUserNames = true,
                RequireUniqueEmail = true
            };

            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 5,
                RequireNonLetterOrDigit = false,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = false
            };

            return manager;
        }
    }
}
