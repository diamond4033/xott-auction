﻿using XoTT_Auction.DAL.Context;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace XoTT_Auction.DAL.Managers
{
    /// <summary>
    /// class to manage roles
    /// </summary>
    public class AuctionRoleManager : RoleManager<IdentityRole>
    {
        /// <summary>
        /// role manager constructor
        /// </summary>
        /// <param name="store"></param>
        public AuctionRoleManager(RoleStore<IdentityRole> store)
            : base(store)
        { }
        /// <summary>
        /// method to create new role manager for Identity
        /// </summary>
        /// <param name="options"></param>
        /// <param name="context"></param>
        /// <returns>new role manager</returns>
        public static AuctionRoleManager Create(
            IdentityFactoryOptions<AuctionRoleManager> options,
            IOwinContext context)
        {
            return new AuctionRoleManager(new
                RoleStore<IdentityRole>(context.Get<AuctionContext>()));
        }
    }
}
