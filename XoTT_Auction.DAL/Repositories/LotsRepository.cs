﻿using XoTT_Auction.DAL.Context;
using XoTT_Auction.DAL.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using System.Reflection;
using System;
using XoTT_Auction.DAL.Interfaces;

namespace XoTT_Auction.DAL.Repositories
{
    /// <inheritdoc cref="ILotsRepository"/>
    public class LotsRepository : ILotsRepository
    {
        /// <summary>
        /// database context
        /// </summary>
        private readonly AuctionContext _auctionContext;
        /// <summary>
        /// constructor with context
        /// </summary>
        /// <param name="auctionContext"></param>
        public LotsRepository(AuctionContext auctionContext) 
        {
            _auctionContext = auctionContext;
        }
        public async Task<Lot> CreateAsync(Lot item)
        {
            _auctionContext.Lots.Add(item);

            await _auctionContext.SaveChangesAsync();

            return item;
        }

        public async Task<List<Lot>> GetAllAsync()
        {
            return await _auctionContext.Lots.ToListAsync();
        }
        public List<Lot> GetAll()
        {
            return _auctionContext.Lots.ToList();
        }

        public async Task<Lot> GetByIdAsync(int id)
        {
            return await _auctionContext.Lots.FindAsync(id);
        }
        public Lot GetById(int id)
        {
            return _auctionContext.Lots.Find(id);
        }

        //public static string AssemblyDirectory
        //{
        //    get
        //    {
        //        string path = Assembly.GetExecutingAssembly().CodeBase;
        //        path = path.Substring(8);

        //        return Path.GetDirectoryName(path);
        //    }
        //}

        public async Task<bool> DeleteAsync(int id)
        {
            var item = _auctionContext.Lots.Find(id);

            var result = item != null;
            if (result)
            {
                //if (item.LotPictures?.Count > 0)
                //{
                //    var pics = item.LotPictures.Select(lp => lp.Picture);
                //    foreach (var pic in pics)
                //    {
                //        var path = AssemblyDirectory;
                //        path = path.Remove(path.Length - 3, 3);
                //        path += "Content\\images\\" + pic.URL;
                //        File.Delete(path);
                //    }
                //    _auctionContext.Pictures.RemoveRange(pics);
                //}
                _auctionContext.LotPictures.RemoveRange(item.LotPictures);
                _auctionContext.Bids.RemoveRange(item.Bids);
                _auctionContext.Entry(item).State = EntityState.Deleted;
                await _auctionContext.SaveChangesAsync();
            }

            return result;
        }

        public async Task<bool> UpdateAsync(Lot item)
        {
            var lot = _auctionContext.Lots.Find(item.Id);

            
            //if(lot.LotPictures?.Count > 0)
            //{
            //    var pics = lot.LotPictures.Select(lp => lp.Picture);
            //    foreach(var pic in pics)
            //    {
            //        var path = AssemblyDirectory;
            //        path = path.Remove(path.Length - 3, 3);
            //        path += "Content\\images\\" + pic.URL;
            //        File.Delete(path);
            //    }
            //    _auctionContext.Pictures.RemoveRange(pics);
            //}

            _auctionContext.Entry(lot).CurrentValues.SetValues(item);
            _auctionContext.Entry(lot).State = EntityState.Modified;

            if (item.LotPictures?.Count > 0)
            {
                _auctionContext.LotPictures.AddRange(item.LotPictures);
            }

            await _auctionContext.SaveChangesAsync();

            return lot != null;
        }
    }
}
