﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using XoTT_Auction.DAL.Context;
using XoTT_Auction.DAL.Interfaces;
using XoTT_Auction.DAL.Models;

namespace XoTT_Auction.DAL.Repositories
{
    /// <inheritdoc cref="ICategoriesRepository"/>
    public class CategoriesRepository : ICategoriesRepository
    {
        /// <summary>
        /// database context
        /// </summary>
        private readonly AuctionContext _auctionContext;
        /// <summary>
        /// constructor with context
        /// </summary>
        /// <param name="auctionContext"></param>
        public CategoriesRepository(AuctionContext auctionContext)
        {
            _auctionContext = auctionContext;
        }
        public async Task<Category> CreateAsync(Category item)
        {
            _auctionContext.Categories.Add(item);
            await _auctionContext.SaveChangesAsync();

            return item;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _auctionContext.Categories.FindAsync(id);
            var result = item != null;
            if (result)
            {
                _auctionContext.Entry(item).State = EntityState.Deleted;
                await _auctionContext.SaveChangesAsync();
            }

            return result;
        }

        public async Task<List<Category>> GetAllAsync()
        {
            return await _auctionContext.Categories
                .Include(c => c.Lots)
                .ToListAsync();
        }
        public List<Category> GetAll()
        {
            return _auctionContext.Categories
                .Include(c => c.Lots)
                .ToList();
        }
        public Category GetById(int id)
        {
            return _auctionContext.Categories
                .Include(c => c.Lots)
                .Where(c => c.Id == id).First();
        }

        public int GetCategoriesCount(string search)
        {
            var categories = _auctionContext.Categories.AsQueryable();

            if (!string.IsNullOrEmpty(search))
            {
                categories = categories
                    .Where(c => c.Name.ToLower()
                    .Contains(search.ToLower()));
            }

            return categories.Count();
        }

        public async Task<bool> UpdateAsync(Category item)
        {
            var category = _auctionContext.Categories.Attach(item);
            _auctionContext.Entry(item).State = EntityState.Modified;
            await _auctionContext.SaveChangesAsync();

            return category != null;
        }
    }
}
