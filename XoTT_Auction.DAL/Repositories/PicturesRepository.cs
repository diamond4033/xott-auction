﻿using System.Threading.Tasks;
using XoTT_Auction.DAL.Context;
using XoTT_Auction.DAL.Interfaces;
using XoTT_Auction.DAL.Models;

namespace XoTT_Auction.DAL.Repositories
{
    /// <inheritdoc cref="IPicturesRepository"/>
    public class PicturesRepository : IPicturesRepository
    {
        /// <summary>
        /// database context
        /// </summary>
        private readonly AuctionContext _auctionContext;
        /// <summary>
        /// constructor with context
        /// </summary>
        /// <param name="auctionContext"></param>
        public PicturesRepository(AuctionContext auctionContext)
        {
            _auctionContext = auctionContext;
        }
        public async Task<int> CreateAsync(Picture item)
        {
            _auctionContext.Pictures.Add(item);

            await _auctionContext.SaveChangesAsync();

            return item.Id;
        }
    }
}
