﻿using System.Linq;
using System.Threading.Tasks;
using XoTT_Auction.DAL.Context;
using XoTT_Auction.DAL.Interfaces;
using XoTT_Auction.DAL.Models;

namespace XoTT_Auction.DAL.Repositories
{
    /// <inheritdoc cref="IBidsRepository"/>
    public class BidsRepository : IBidsRepository
    {
        /// <summary>
        /// database context
        /// </summary>
        private readonly AuctionContext _auctionContext;
        /// <summary>
        /// constructor with context
        /// </summary>
        /// <param name="auctionContext"></param>
        public BidsRepository(AuctionContext auctionContext)
        {
            _auctionContext = auctionContext;
        }
        public async Task<bool> CreateAsync(Bid item)
        {
            _auctionContext.Bids.Add(item);

            int countOfChanges = await _auctionContext.SaveChangesAsync();

            return countOfChanges > 0;
        }

        public int GetAllCount()
        {
            return _auctionContext.Bids.Count();
        }
    }
}
