﻿using XoTT_Auction.DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace XoTT_Auction.DAL.Interfaces
{
    /// <summary>
    /// Lots CRUD operations
    /// </summary>
    public interface ILotsRepository
    {
        /// <summary>
        /// Create new lot async
        /// </summary>
        /// <param name="item"></param>
        /// <returns>new lot</returns>
        Task<Lot> CreateAsync(Lot item);
        /// <summary>
        /// Get all lots async
        /// </summary>
        /// <returns>all lots</returns>
        Task<List<Lot>> GetAllAsync();
        /// <summary>
        /// Get all lots sync
        /// </summary>
        /// <returns>all lots</returns>
        List<Lot> GetAll();
        /// <summary>
        /// Get lot by lot id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>lot</returns>
        Lot GetById(int id);
        /// <summary>
        /// Get lot by lot id async
        /// </summary>
        /// <param name="id"></param>
        /// <returns>lot</returns>
        Task<Lot> GetByIdAsync(int id);
        /// <summary>
        /// Update existing lot
        /// </summary>
        /// <param name="item"></param>
        /// <returns>bool</returns>
        Task<bool> UpdateAsync(Lot item);
        /// <summary>
        /// Delete existing lot
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool</returns>
        Task<bool> DeleteAsync(int id);
    }
}
