﻿using System.Threading.Tasks;
using XoTT_Auction.DAL.Models;

namespace XoTT_Auction.DAL.Interfaces
{
    /// <summary>
    /// Repository for bids creating and counting
    /// </summary>
    public interface IBidsRepository
    {
        /// <summary>
        /// Creating new bid
        /// </summary>
        /// <param name="item"></param>
        /// <returns>bool</returns>
        Task<bool> CreateAsync(Bid item);
        /// <summary>
        /// Count all bids
        /// </summary>
        /// <returns>int</returns>
        int GetAllCount();
    }
}
