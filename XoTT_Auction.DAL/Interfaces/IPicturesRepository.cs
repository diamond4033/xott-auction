﻿using System.Threading.Tasks;
using XoTT_Auction.DAL.Models;

namespace XoTT_Auction.DAL.Interfaces
{
    /// <summary>
    /// Repository to save new picture
    /// </summary>
    public interface IPicturesRepository
    {
        /// <summary>
        /// Save new picture async
        /// </summary>
        /// <param name="item"></param>
        /// <returns>int</returns>
        Task<int> CreateAsync(Picture item);
    }
}
