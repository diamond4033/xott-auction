﻿using System.Collections.Generic;
using System.Threading.Tasks;
using XoTT_Auction.DAL.Models;

namespace XoTT_Auction.DAL.Interfaces
{
    /// <summary>
    /// Category CRUD operations
    /// </summary>
    public interface ICategoriesRepository
    {
        /// <summary>
        /// Create new category async
        /// </summary>
        /// <param name="item"></param>
        /// <returns>new category</returns>
        Task<Category> CreateAsync(Category item);
        /// <summary>
        /// Get all categories async
        /// </summary>
        /// <returns>all categories</returns>
        Task<List<Category>> GetAllAsync();
        /// <summary>
        /// Get all categories sync
        /// </summary>
        /// <returns>all categories</returns>
        List<Category> GetAll();
        /// <summary>
        /// Get category by category id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Category</returns>
        Category GetById(int id);
        /// <summary>
        /// Count all categories
        /// </summary>
        /// <param name="search"></param>
        /// <returns>int</returns>
        int GetCategoriesCount(string search);
        /// <summary>
        /// Update existing category
        /// </summary>
        /// <param name="item"></param>
        /// <returns>bool</returns>
        Task<bool> UpdateAsync(Category item);
        /// <summary>
        /// Delete existing category
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool</returns>
        Task<bool> DeleteAsync(int id);
    }
}
