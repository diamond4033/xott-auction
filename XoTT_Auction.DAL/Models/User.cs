﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;

namespace XoTT_Auction.DAL.Models
{
    /// <summary>
    /// Custom Identity user class
    /// </summary>
    public class User : IdentityUser
    {
        [Required]
        [MaxLength(40)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(40)]
        public string LastName { get; set; }

        [MaxLength(260)]
        public string PicUrl { get; set; }
    }
}
