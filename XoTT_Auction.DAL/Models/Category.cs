﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace XoTT_Auction.DAL.Models
{
    /// <summary>
    /// Lot category
    /// </summary>
    public class Category : BaseEntity
    {
        [Required]
        [MinLength(10), MaxLength(125)]
        public string Name { get; set; }
        [MaxLength(300)]
        public string Description { get; set; }

        public virtual List<Lot> Lots { get; set; }
    }
}
