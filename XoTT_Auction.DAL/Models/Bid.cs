﻿using System;

namespace XoTT_Auction.DAL.Models
{
    /// <summary>
    /// lot bid
    /// </summary>
    public class Bid : BaseEntity
    {
        public decimal BidAmount { get; set; }
        public DateTime Timestamp { get; set; }

        public int LotId { get; set; }
        public virtual Lot Lot { get; set; }

        public string UserId { get; set; }
        public virtual User User { get; set; }
    }
}
