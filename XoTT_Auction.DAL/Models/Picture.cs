﻿namespace XoTT_Auction.DAL.Models
{
    /// <summary>
    /// Picture URL class
    /// </summary>
    public class Picture : BaseEntity
    {
        public string URL { get; set; }
    }
}
