﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace XoTT_Auction.DAL.Models
{
    /// <summary>
    /// Lot class
    /// </summary>
    public class Lot : BaseEntity
    {
        [Required]
        [MinLength(15), MaxLength(150)]
        public string Title { get; set; }
        public string Description { get; set; }

        public decimal StartPrice { get; set; }
        public decimal Step { get; set; }

        public DateTime? StartingTime { get; set; }
        public DateTime? EndingTime { get; set; }
        public virtual List<LotPicture> LotPictures { get; set; }

        public virtual List<Bid> Bids { get; set; }

        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public string UserId { get; set; }
        public virtual User User { get; set; }
    }
}
