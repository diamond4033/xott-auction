﻿using System.ComponentModel.DataAnnotations;

namespace XoTT_Auction.DAL.Models
{
    /// <summary>
    /// intermediate class for linking lots and pictures
    /// </summary>
    public class LotPicture : BaseEntity
    {
        [Required]
        public int LotId { get; set; }
        [Required]
        public int PictureId { get; set; }
        public virtual Picture Picture { get; set; }
    }
}
