﻿using XoTT_Auction.DAL.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace XoTT_Auction.DAL.Context
{
    /// <summary>
    /// Database context class
    /// </summary>
    public class AuctionContext : IdentityDbContext<User>
    {
        /// <summary>
        /// Lots dbset
        /// </summary>
        public DbSet<Lot> Lots { get; set; }
        /// <summary>
        /// Bids dbset
        /// </summary>
        public DbSet<Bid> Bids { get; set; }
        /// <summary>
        /// Categories dbset
        /// </summary>
        public DbSet<Category> Categories { get; set; }
        /// <summary>
        /// Pictures dbset
        /// </summary>
        public DbSet<Picture> Pictures { get; set; }
        /// <summary>
        /// intermediate class for linking lots and pictures
        /// </summary>
        public DbSet<LotPicture> LotPictures { get; set; }
        /// <summary>
        /// constructor to set database initializer
        /// </summary>
        public AuctionContext() : base("name=AuctionContext") 
        {
            Database.SetInitializer<AuctionContext>(null);
        }
        /// <summary>
        /// Methods for creating new database context for Identity
        /// </summary>
        /// <returns></returns>
        public static AuctionContext Create() 
        {
            return new AuctionContext();
        }
        /// <summary>
        /// executed when models change
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));
        }
    }
}
