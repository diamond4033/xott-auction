﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;
using XoTT_Auction.DAL.Managers;
using XoTT_Auction.DAL.Models;

namespace XoTT_Auction.DAL.Context
{
    /// <summary>
    /// database initializer class
    /// </summary>
    public class AuctionDbInitializer : IDatabaseInitializer<AuctionContext>
    {
        /// <summary>
        /// invokes methods to initialize database
        /// </summary>
        /// <param name="context"></param>
        public void InitializeDatabase(AuctionContext context)
        {
            SeedRoles(context);
            SeedUsers(context);
        }
        /// <summary>
        /// create roles if not exist
        /// </summary>
        /// <param name="context"></param>
        public void SeedRoles(AuctionContext context)
        {
            var rolesInAuction = new List<IdentityRole>();

            rolesInAuction.Add(new IdentityRole() { Name = "Admin" });
            rolesInAuction.Add(new IdentityRole() { Name = "User" });

            var roleStore = new RoleStore<IdentityRole>(context);
            var rolesManager = new AuctionRoleManager(roleStore);

            rolesManager.RoleValidator = new RoleValidator<IdentityRole>(rolesManager);

            foreach (IdentityRole role in rolesInAuction)
            {
                if (!rolesManager.RoleExists(role.Name))
                {
                    rolesManager.Create(role);
                }
            }
        }
        /// <summary>
        /// create admin user if not exist
        /// </summary>
        /// <param name="context"></param>
        public void SeedUsers(AuctionContext context)
        {
            var usersStore = new UserStore<User>(context);
            var usersManager = new AuctionUserManager(usersStore);

            usersManager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 4,
                RequireDigit = false,
                RequireLowercase = true,
                RequireNonLetterOrDigit = false,
                RequireUppercase = false
            };

            var admin = new User
            {
                Email = "xottamail@gmail.com",
                UserName = "admin",
                FirstName = "Admin",
                LastName = "nimdA"
            };
            string password = "admin";

            if (usersManager.FindByEmail(admin.Email) == null)
            {
                var result = usersManager.Create(admin, password);

                if (result.Succeeded)
                {
                    usersManager.AddToRole(admin.Id, "Admin");
                    usersManager.AddToRole(admin.Id, "User");
                }
            }
        }
    }
}
