﻿using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Text;
using System.Web.Mvc;
using XoTT_Auction.Web.ViewModels;

namespace XoTT_Auction.Web.Filters
{
    /// <summary>
    /// Attribute to catch validation errors
    /// </summary>
    public class ValidationExceptionAttribute : FilterAttribute, IExceptionFilter
    {
        /// <summary>
        /// Validation error handling method
        /// </summary>
        /// <param name="filterContext"></param>
        public void OnException(ExceptionContext filterContext)
        {
            List<string> errors = new List<string>();
            errors.Add(filterContext.Exception.Message);
            if (filterContext.Exception is DbEntityValidationException e)
            {
                var errorString = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    errorString.Append($"Entity of type" +
                        $" \"{eve.Entry.Entity.GetType().Name}\"" +
                        $" in state \"{eve.Entry.State}\" " +
                        $"has the following validation errors: ");
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errorString.Append($"- Property: \"{ve.PropertyName}\"" +
                            $", Error: \"{ve.ErrorMessage}\"");
                    }
                    errors.Add(errorString.ToString());
                }
            }
            var model = new ListOfErrorsViewModel
            {
                PageTitle = "Error Page",
                PageDescription = "This is Error Page",
                Errors = errors
            };
            filterContext.Result = new ViewResult 
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(filterContext.Controller.ViewData) 
                {
                    Model = model
                }
            };
            filterContext.ExceptionHandled = true;
        }
    }
}