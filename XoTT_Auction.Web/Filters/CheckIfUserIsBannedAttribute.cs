﻿using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using XoTT_Auction.DAL.Managers;
using Microsoft.AspNet.Identity.Owin;
using XoTT_Auction.Web.ViewModels;

namespace XoTT_Auction.Web.Filters
{
    /// <summary>
    /// Class to check if user is banned
    /// </summary>
    public class CheckIfUserIsBannedAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Method to check if a user is banned
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                var userManager = filterContext.HttpContext.GetOwinContext().GetUserManager<AuctionUserManager>();
                var userId = filterContext.HttpContext.User.Identity.GetUserId();
                if (userManager.IsLockedOut(userId))
                {
                    var model = new ListOfErrorsViewModel
                    {
                        PageTitle = "Error Page",
                        PageDescription = "This is Error Page",
                        Errors = new string[] { "Sorry, you have been banned..." }
                    };
                    filterContext.Result = new ViewResult
                    {
                        ViewName = "Error",
                        ViewData = new ViewDataDictionary(filterContext.Controller.ViewData)
                        {
                            Model = model
                        }
                    };
                }
            }
        }
    }
}