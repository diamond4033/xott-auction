﻿using System.Threading.Tasks;
using System.Web.Mvc;
using XoTT_Auction.Web.ViewModels;
using XoTT_Auction.BLL.Interfaces;

namespace XoTT_Auction.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILotsService _lotsService;
        public HomeController(ILotsService lotService)
        {
            _lotsService = lotService;
        }
        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
            try
            {
                var lots = await _lotsService.GetAllLotsAsync();
                var list = new LotsListingViewModel
                {
                    PageTitle = "Home Page",
                    PageDescription = "This is Home Page",
                    Lots = lots
                };
                return View(list);
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
        }
    }
}