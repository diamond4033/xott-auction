﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;
using XoTT_Auction.BLL.Interfaces;
using XoTT_Auction.DAL.Models;

namespace XoTT_Auction.Web.Controllers
{
    [Authorize]
    public class PicturesController : Controller
    {
        private readonly IPicturesService _pictureService;
        public PicturesController(IPicturesService pictureService)
        {
            _pictureService = pictureService;
        }

        [HttpPost]
        public async Task<JsonResult> UploadPictures()
        {
            try
            {
                JsonResult result = new JsonResult();

                List<object> picturesJSON = new List<object>();

                var pictures = Request.Files;

                for (int i = 0; i < pictures.Count; i++)
                {
                    var picture = pictures[i];

                    var fileName = Guid.NewGuid() + Path.GetExtension(picture.FileName);

                    var path = Server.MapPath("~/Content/images/") + fileName;

                    picture.SaveAs(path);

                    var dbPicture = new Picture
                    {
                        URL = fileName
                    };

                    int pictureId = await _pictureService.SavePictureAsync(dbPicture);

                    picturesJSON.Add(new { Id = pictureId, URL = fileName });
                }

                result.Data = picturesJSON;

                return result;
            }
            catch
            {
                JsonResult result = new JsonResult();
                result.Data = new { success = false, message = "An error has occurred, please try again" };
                return result;
            }
        }
    }
}