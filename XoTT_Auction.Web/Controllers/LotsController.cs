﻿using System.Threading.Tasks;
using System.Web.Mvc;
using XoTT_Auction.DAL.Models;
using XoTT_Auction.Web.ViewModels;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using XoTT_Auction.Web.Filters;
using XoTT_Auction.BLL.Interfaces;

namespace XoTT_Auction.Web.Controllers
{
    [Authorize]
    [CheckIfUserIsBanned]
    public class LotsController : Controller
    {
        private readonly ILotsService _lotsService;
        private readonly ICategoriesService _categoriesService;
        public LotsController(ILotsService lotService,
            ICategoriesService categoriesService) 
        {
            _lotsService = lotService;
            _categoriesService = categoriesService;
        }

        public ActionResult Index(int? categoryId, string searchTerm, int? pageNo)
        {
            try
            {
                var lots = new LotsListingViewModel
                {
                    PageTitle = "Lots",
                    PageDescription = "This is Lots Page",

                    CategoryId = categoryId,
                    SearchTerm = searchTerm,
                    PageNo = pageNo
                };
                lots.Categories = _categoriesService.GetAll();
                return View(lots);
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
        }

        public PartialViewResult Listing(int? categoryId, string searchTerm, int? pageNo) 
        {
            try
            {
                var pageSize = 5;
                pageNo = pageNo ?? 1;

                string userId = null;
                if (!User.IsInRole("Admin"))
                {
                    userId = User.Identity.GetUserId();
                }

                var lotsModel = new LotsListingViewModel
                {
                    Lots = _lotsService.FilterLots(categoryId, pageNo, pageSize, searchTerm, userId)
                };

                var totalLots = _lotsService.GetFilteredLotsCount(categoryId, searchTerm);

                lotsModel.Pager = new Pager(totalLots, pageNo, pageSize);

                lotsModel.CategoryId = categoryId;
                lotsModel.SearchTerm = searchTerm;

                return PartialView(lotsModel);
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return PartialView("Error", error);
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> Details(int id)
        {
            try
            {
                var model = new LotDetailsViewModel
                {
                    PageTitle = "Details Page",
                    PageDescription = "This is Details Page",
                    Lot = await _lotsService.GetLotByIdAsync(id)
                };
                model.BidsAmount = model.Lot.StartPrice
                    + model.Lot.Bids.Sum(b => b.BidAmount);
                var latestBidder = model.Lot.Bids
                    .OrderByDescending(b => b.Timestamp)
                    .FirstOrDefault();
                model.LatestBidder = latestBidder?.User;
                return View(model);
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }        
        }



        [Authorize(Roles = "User")]
        public async Task<ActionResult> Create()
        {
            try
            {
                var model = new LotViewModel
                {
                    Categories = await _categoriesService.GetAllAsync()
                };
                return PartialView(model);
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
        }



        [HttpPost]
        [Authorize(Roles = "User")]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Create(LotViewModel model)
        {
            try
            {
                JsonResult result = new JsonResult();

                if (ModelState.IsValid)
                {

                    Lot newLot = new Lot
                    {
                        Title = model.Title,
                        Description = model.Description,
                        StartPrice = model.StartPrice,
                        Step = model.Step,
                        StartingTime = model.StartingTime,
                        EndingTime = model.EndingTime,
                        CategoryId = model.CategoryId,
                        UserId = User.Identity.GetUserId()
                    };

                    if (!string.IsNullOrEmpty(model.LotPictures))
                    {
                        var pictureIds = model.LotPictures.Split(',').Select(int.Parse);
                        var list = new List<LotPicture>();
                        list.AddRange(pictureIds
                            .Select(pi => new LotPicture() { PictureId = pi }));
                        newLot.LotPictures = list;
                    }
                    await _lotsService.CreateLotAsync(newLot);

                    result.Data = new { success = true };
                }
                else
                {
                    result.Data = new { success = false, message = "Invalid inputs." };
                }

                return result;
            }
            catch
            {
                JsonResult result = new JsonResult();
                result.Data = new { success = false, message = "An error has occurred, please try again" };
                return result;
            }
            
        }


        [Authorize(Roles = "User")]
        public async Task<ActionResult> Edit(int id)
        {
            try
            {
                var model = new LotViewModel
                {
                    Lot = await _lotsService.GetLotByIdAsync(id),
                    Categories = await _categoriesService.GetAllAsync()
                };

                return PartialView(model);
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
        }




        [HttpPost]
        [Authorize(Roles = "User")]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Edit(LotViewModel model)
        {
            try
            {
                JsonResult result = new JsonResult();

                if (ModelState.IsValid)
                {
                    var lot = new Lot
                    {
                        Id = model.Id,
                        Title = model.Title,
                        Description = model.Description,
                        StartPrice = model.StartPrice,
                        Step = model.Step,
                        StartingTime = model.StartingTime,
                        EndingTime = model.EndingTime,
                        CategoryId = model.CategoryId,
                        UserId = User.Identity.GetUserId()
                    };

                    if (!string.IsNullOrEmpty(model.LotPictures))
                    {
                        var pictureIds = model.LotPictures.Split(',').Select(int.Parse);

                        var list = new List<LotPicture>();
                        list.AddRange(pictureIds.Select(pi => new LotPicture()
                        {
                            PictureId = pi,
                            LotId = lot.Id
                        }).ToList());
                        lot.LotPictures = list;
                    }

                    await _lotsService.UpdateLotAsync(lot);

                    result.Data = new { success = true };
                }
                else
                {
                    result.Data = new { success = false, message = "Invalid inputs." };
                }

                return result;
            }
            catch
            {
                JsonResult result = new JsonResult();
                result.Data = new { success = false, message = "An error has occurred, please try again" };
                return result;
            }
            
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _lotsService.DeleteLotAsync(id);
                return RedirectToAction("Listing");
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
        }
    }
}
