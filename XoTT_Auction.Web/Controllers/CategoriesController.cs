﻿using System.Threading.Tasks;
using System.Web.Mvc;
using XoTT_Auction.BLL.Interfaces;
using XoTT_Auction.DAL.Models;
using XoTT_Auction.Web.ViewModels;

namespace XoTT_Auction.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CategoriesController : Controller
    {
        private readonly ICategoriesService _categoriesService;
        public CategoriesController( ICategoriesService categoriesService)
        {
            _categoriesService = categoriesService;
        }
        public ActionResult Index(string searchTerm)
        {
            try
            {
                var catsModel = new CategoriesListingViewModel
                {
                    PageTitle = "Categories",
                    PageDescription = "Categories listing page.",
                    SearchTerm = searchTerm
                };

                return View(catsModel);
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
        }

        public PartialViewResult Listing(string searchTerm, int? pageNo)
        {
            try
            {
                int pageSize = 5;
                pageNo = pageNo ?? 1;

                var catsModel = new CategoriesListingViewModel
                {
                    AllCategories = _categoriesService.FilterCategories(searchTerm, pageNo.Value, pageSize)
                };

                var totalCount = _categoriesService.GetCategoriesCount(searchTerm);
                catsModel.Pager = new Pager(totalCount, pageNo.Value, pageSize);

                catsModel.SearchTerm = searchTerm;

                return PartialView(catsModel);
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return PartialView("Error", error);
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            try
            {
                return PartialView();
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
        }

        [HttpPost]
        public async Task<JsonResult> Create(CategoryViewModel catModel)
        {
            try
            {
                JsonResult result = new JsonResult();

                if (ModelState.IsValid)
                {
                    Category newCategory = new Category
                    {
                        Name = catModel.Name,
                        Description = catModel.Description
                    };

                    await _categoriesService.CreateAsync(newCategory);

                    result.Data = new { success = true };
                }
                else
                {
                    result.Data = new { success = false, message = "Invalid inputs." };
                }

                return result;
            }
            catch
            {
                JsonResult result = new JsonResult();
                result.Data = new { success = false, message = "An error has occurred, please try again" };
                return result;
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            try
            {
                var model = new CategoryViewModel
                {
                    Category = _categoriesService.GetById(id)
                };

                return PartialView(model);
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
        }

        [HttpPost]
        public async Task<JsonResult> Edit(CategoryViewModel categoryModel)
        {
            try
            {
                JsonResult result = new JsonResult();

                if (ModelState.IsValid)
                {
                    var categoryFromDb = _categoriesService.GetById(categoryModel.Id);

                    categoryFromDb.Name = categoryModel.Name;
                    categoryFromDb.Description = categoryModel.Description;

                    await _categoriesService.UpdateAsync(categoryFromDb);

                    result.Data = new { success = true };
                }
                else
                {
                    result.Data = new { success = false, message = "Invalid inputs." };
                }

                return result;
            }
            catch
            {
                JsonResult result = new JsonResult();
                result.Data = new { success = false, message = "An error has occurred, please try again" };
                return result;
            }
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            try
            {
                var model = new CategoryDetailsViewModel
                {
                    Category = _categoriesService.GetById(id)
                };

                model.PageTitle = model.Category.Name;
                if (model.Category.Description != null)
                {
                    if (model.Category.Description.Length > 10)
                        model.Category.Description = model.Category.Description.Substring(0, 10);
                    else
                        model.Category.Description = "Category details.";
                }

                return View(model);
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(Category category)
        {
            try
            {
                await _categoriesService.DeleteAsync(category.Id);

                return RedirectToAction("Listing");
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
        }
    }
}