﻿using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using XoTT_Auction.Web.ViewModels;
using XoTT_Auction.BLL.Services;
using XoTT_Auction.BLL.Dto;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using XoTT_Auction.DAL.Managers;
using XoTT_Auction.BLL.Interfaces;

namespace XoTT_Auction.Web.Controllers
{
    [Authorize]
    public class AccountsController : Controller
    {
        private IAccountsService _accountsService
        {
            get 
            {
                return new AccountsService(HttpContext.GetOwinContext().GetUserManager<AuctionUserManager>());
            }
        }

        [AllowAnonymous]
        public ActionResult Register() 
        {
            try
            {
                var vModel = new RegisterViewModel
                {
                    PageTitle = "Registration Page",
                    PageDescription = "This is Registration Page"
                };
                return View(vModel);
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel registerViewModel) 
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var regInfo = new RegistrationInfoDto
                    {
                        FirstName = registerViewModel.FirstName,
                        LastName = registerViewModel.LastName,
                        Username = registerViewModel.Username,
                        Email = registerViewModel.Email,
                        Password = registerViewModel.Password
                    };
                    IdentityResult result = await _accountsService.RegisterNewUser(regInfo);
                    if (result.Succeeded)
                        return RedirectToAction("Login", "Accounts");
                    else
                    {
                        foreach (string error in result.Errors)
                        {
                            ModelState.AddModelError("", error);
                        }
                    }
                }
                return View(registerViewModel);
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
            
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            try
            {
                if (!User.Identity.IsAuthenticated)
                {
                    ViewBag.returnUrl = returnUrl;
                    var model = new LoginViewModel
                    {
                        PageTitle = "Login Page",
                        PageDescription = "This is Login Page",
                        ReturnUrl = returnUrl
                    };
                    return View(model);
                }
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "You are not allowed to do this action." }
                };
                return View("Error", error);
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel loginInfo)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    LoginInfoDto loginInfoDto = new LoginInfoDto { Username = loginInfo.Username, Password = loginInfo.Password };
                    ClaimsIdentity claim = await _accountsService.Authenticate(loginInfoDto);

                    if (claim == null)
                    {
                        ModelState.AddModelError("", "Incorrect username or password");
                    }
                    else
                    {

                        AuthManager.SignOut();
                        AuthManager.SignIn(new AuthenticationProperties
                        {
                            IsPersistent = true
                        }, claim);
                        if (string.IsNullOrEmpty(loginInfo.ReturnUrl))
                            return RedirectToAction("Index", "Home");
                        return Redirect(loginInfo.ReturnUrl);
                    }
                }
                loginInfo.PageTitle = "Login Page";
                loginInfo.PageDescription = "This is Login Page";
                return View(loginInfo);
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult Logout()
        {
            try
            {
                AuthManager.SignOut();

                return RedirectToAction("Index", "Home");
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
        }
        private IAuthenticationManager AuthManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }
}
