﻿using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using XoTT_Auction.BLL.Interfaces;
using XoTT_Auction.BLL.Services;
using XoTT_Auction.DAL.Managers;
using XoTT_Auction.DAL.Models;
using XoTT_Auction.Web.ViewModels;

namespace XoTT_Auction.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private IUsersService _usersService
        {
            get
            {
                return new UsersService(HttpContext.GetOwinContext().GetUserManager<AuctionUserManager>());
            }
        }
        public ActionResult Index(string searchTerm)
        {
            try
            {
                var usersModel = new UsersListingViewModel
                {
                    PageTitle = "Users",
                    PageDescription = "Users listing page.",
                    SearchTerm = searchTerm
                };

                return View(usersModel);
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
        }

        public PartialViewResult Listing(string searchTerm, int? pageNo)
        {
            try
            {
                int pageSize = 5;
                pageNo = pageNo ?? 1;

                var usersModel = new UsersListingViewModel
                {
                    AllUsers = _usersService.FilterUsers(searchTerm, pageNo.Value, pageSize)
                };

                var totalCount = _usersService.GetUsersCount(searchTerm);
                usersModel.Pager = new Pager(totalCount, pageNo.Value, pageSize);

                usersModel.SearchTerm = searchTerm;

                return PartialView(usersModel);
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return PartialView("Error", error);
            }
        }

        [HttpGet]
        public ActionResult Details(string id)
        {
            try
            {
                var model = new UserDetailsViewModel
                {
                    User = _usersService.GetUserById(id)
                };

                model.PageTitle = "User Details";

                return View(model);
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> BanUser(User user)
        {
            try
            {
                await _usersService.UserBanToggle(user.Id);

                return RedirectToAction("Listing");
            }
            catch
            {
                var error = new ListOfErrorsViewModel
                {
                    PageTitle = "Error Page",
                    PageDescription = "This is Error Page",
                    Errors = new string[] { "An error has occurred, please try again" }
                };
                return View("Error", error);
            }
        }
    }
}