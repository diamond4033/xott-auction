﻿using Microsoft.AspNet.Identity;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using XoTT_Auction.BLL.Interfaces;
using XoTT_Auction.DAL.Models;

namespace XoTT_Auction.Web.Controllers
{
    [Authorize]
    public class BidsController : Controller
    {
        private readonly IBidsService _bidsService;
        private readonly ILotsService _lotsService;
        public BidsController(IBidsService bidsService, ILotsService lotsService)
        {
            _bidsService = bidsService;
            _lotsService = lotsService;
        }
        [Authorize(Roles = "User")]
        public async Task<JsonResult> Bid(int lotId)
        {
            try
            {
                JsonResult result = new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };

                var lot = _lotsService.GetLotById(lotId);

                Bid bid = new Bid
                {
                    LotId = lotId,
                    UserId = User.Identity.GetUserId(),
                    Timestamp = DateTime.Now,
                    BidAmount = lot.Step
                };

                var bidResult = await _bidsService.CreateAsync(bid);

                if (bidResult)
                {
                    result.Data = new { success = true };
                }
                else
                {
                    result.Data = new { success = false, message = "Failed to add a bid" };
                }
                return result;
            }
            catch
            {
                JsonResult result = new JsonResult();
                result.Data = new { success = false, message = "An error has occurred, please try again" };
                return result;
            }
        }
    }
}