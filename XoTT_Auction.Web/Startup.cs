﻿using XoTT_Auction.DAL.Context;
using XoTT_Auction.DAL.Managers;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

[assembly: OwinStartup(typeof(XoTT_Auction.Web.Startup))]

namespace XoTT_Auction.Web
{
    /// <summary>
    /// Identity configuration class
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Identity configuration
        /// </summary>
        /// <param name="app"></param>
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext<AuctionContext>(AuctionContext.Create);
            app.CreatePerOwinContext<AuctionUserManager>(AuctionUserManager.Create);
            app.CreatePerOwinContext<AuctionRoleManager>(AuctionRoleManager.Create);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Accounts/Login"),
                LogoutPath = new PathString("/Home/Index")
            });
        }
    }
}
