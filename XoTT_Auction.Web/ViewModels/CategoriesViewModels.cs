﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using XoTT_Auction.DAL.Models;

namespace XoTT_Auction.Web.ViewModels
{
    /// <summary>
    /// class to display all categories
    /// </summary>
    public class CategoriesListingViewModel : PageViewModel
    {
        public List<Category> AllCategories { get; set; }
        
        public Pager Pager { get; set; }
        public string SearchTerm { get; set; }
    }
    /// <summary>
    /// class to display one category
    /// </summary>
    public class CategoryDetailsViewModel : PageViewModel
    {
        public Category Category { get; set; }
    }
    /// <summary>
    /// class for creating and editing categories
    /// </summary>
    public class CategoryViewModel : PageViewModel
    {
        public int Id { get; set; }

        [Required]
        [MinLength(10), MaxLength(125)]
        public string Name { get; set; }

        [MaxLength(400)]
        public string Description { get; set; }

        public List<Lot> Lots { get; set; }

        public Category Category { get; set; }
    }
}