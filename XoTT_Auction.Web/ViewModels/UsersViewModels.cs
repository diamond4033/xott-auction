﻿using System.Collections.Generic;
using XoTT_Auction.DAL.Models;

namespace XoTT_Auction.Web.ViewModels
{
    /// <summary>
    /// class to display all users
    /// </summary>
    public class UsersListingViewModel : PageViewModel
    {
        public List<User> AllUsers { get; set; }

        public Pager Pager { get; set; }
        public string SearchTerm { get; set; }
    }
    /// <summary>
    /// class to display one user
    /// </summary>
    public class UserDetailsViewModel : PageViewModel
    {
        public User User { get; set; }
    }
}