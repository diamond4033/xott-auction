﻿using XoTT_Auction.DAL.Models;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;

namespace XoTT_Auction.Web.ViewModels
{
    /// <summary>
    /// class to display all lots and categories
    /// </summary>
    public class LotsListingViewModel : PageViewModel
    {
        public List<Lot> Lots { get; set; }

        public Pager Pager { get; set; }

        public string SearchTerm { get; set; }
        public int? PageNo { get; set; }
        public int? CategoryId { get; set; }

        public List<Category> Categories { get; set; }
    }
    /// <summary>
    /// class to display one lot and bid
    /// </summary>
    public class LotDetailsViewModel : PageViewModel
    {
        public Lot Lot { get; set; }
        public decimal BidsAmount { get; set; }

        public User LatestBidder { get; set; }
    }
    /// <summary>
    /// class for creating and editing lots
    /// </summary>
    public class LotViewModel : PageViewModel
    {
        public int Id { get; set; }
        [Required]
        [MinLength(15), MaxLength(150)]
        public string Title { get; set; }
        public string Description { get; set; }
        [Required]
        [Range(1, 15000)]
        public decimal StartPrice { get; set; }
        [Required]
        [Range(1, 10)]
        public decimal Step { get; set; }

        public DateTime? StartingTime { get; set; }
        public DateTime? EndingTime { get; set; }
        public string LotPictures { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public virtual Lot Lot { get; set; }
        public virtual List<Category> Categories { get; set; }
    }
}