﻿using XoTT_Auction.BLL.Services;
using XoTT_Auction.DAL.Repositories;
using Ninject.Modules;
using XoTT_Auction.DAL.Interfaces;
using XoTT_Auction.BLL.Interfaces;

namespace XoTT_Auction.Web
{
    /// <summary>
    /// DI binding class
    /// </summary>
    public class DependencyResolverModule : NinjectModule
    {
        /// <summary>
        /// DI binding method
        /// </summary>
        public override void Load()
        {
            Bind<ILotsRepository>().To<LotsRepository>();
            Bind<ILotsService>().To<LotsService>();
            Bind<IPicturesRepository>().To<PicturesRepository>();
            Bind<IPicturesService>().To<PicturesService>();
            Bind<IBidsRepository>().To<BidsRepository>();
            Bind<IBidsService>().To<BidsService>();
            Bind<ICategoriesRepository>().To<CategoriesRepository>();
            Bind<ICategoriesService>().To<CategoriesService>();
            Bind<IUsersService>().To<UsersService>();
            Bind<IAccountsService>().To<AccountsService>();
        }
    }
}