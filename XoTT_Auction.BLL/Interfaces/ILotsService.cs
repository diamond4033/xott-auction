﻿using XoTT_Auction.DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace XoTT_Auction.BLL.Interfaces
{
    /// <summary>
    /// Lots CRUD operations
    /// </summary>
    public interface ILotsService
    {
        /// <summary>
        /// Create new lot async
        /// </summary>
        /// <param name="item"></param>
        /// <returns>new lot</returns>
        Task<Lot> CreateLotAsync(Lot item);
        /// <summary>
        /// Get all lots async
        /// </summary>
        /// <returns>all lots</returns>
        Task<List<Lot>> GetAllLotsAsync();
        /// <summary>
        /// Get all lots sync
        /// </summary>
        /// <returns>all lots</returns>
        List<Lot> GetAllLots();
        /// <summary>
        /// Get lot by lot id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>lot</returns>
        Lot GetLotById(int id);
        /// <summary>
        /// Get lot by lot id async
        /// </summary>
        /// <param name="id"></param>
        /// <returns>lot</returns>
        Task<Lot> GetLotByIdAsync(int id);
        /// <summary>
        /// Get all lots count
        /// </summary>
        /// <returns>int</returns>
        int GetAllLotsCount();
        /// <summary>
        /// Get all lots filtered by search and category count
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="searchTerm"></param>
        /// <returns>int</returns>
        int GetFilteredLotsCount(int? categoryId, string searchTerm);
        /// <summary>
        /// Get all lots filtered by search, category, userId (if present), and pagination
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="searchTerm"></param>
        /// <param name="userId"></param>
        /// <returns>filtered lots</returns>
        List<Lot> FilterLots(int? categoryId, int? pageNo, int pageSize, string searchTerm, string userId);
        /// <summary>
        /// Update existing lot async
        /// </summary>
        /// <param name="item"></param>
        /// <returns>bool</returns>
        Task<bool> UpdateLotAsync(Lot item);
        /// <summary>
        /// Delete existing lot async
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool</returns>
        Task<bool> DeleteLotAsync(int id);
    }
}
