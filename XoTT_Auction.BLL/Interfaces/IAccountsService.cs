﻿using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Threading.Tasks;
using XoTT_Auction.BLL.Dto;

namespace XoTT_Auction.BLL.Interfaces
{
    /// <summary>
    /// Service for login and registration operations
    /// </summary>
    public interface IAccountsService
    {
        /// <summary>
        /// Register new user with 'User' role
        /// </summary>
        /// <param name="registrationInfo"></param>
        /// <returns>IdentityResult</returns>
        Task<IdentityResult> RegisterNewUser(RegistrationInfoDto registrationInfo);
        /// <summary>
        /// Login into existing account
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <returns>ClaimsIdentity</returns>
        Task<ClaimsIdentity> Authenticate(LoginInfoDto loginInfo);
    }
}
