﻿using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using XoTT_Auction.DAL.Models;
using System.Collections.Generic;

namespace XoTT_Auction.BLL.Interfaces
{
    /// <summary>
    /// Service for receiving, count, and banning users
    /// </summary>
    public interface IUsersService
    {
        /// <summary>
        /// Get all users filtered by search and pagination
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <returns>List of users</returns>
        List<User> FilterUsers(string searchTerm, int pageNo, int pageSize);
        /// <summary>
        /// Count all searched users
        /// </summary>
        /// <returns>int</returns>
        int GetUsersCount(string search);
        /// <summary>
        /// Get user by user id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>user</returns>
        User GetUserById(string id);
        /// <summary>
        /// method to ban existing user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>IdentityResult</returns>
        Task<IdentityResult> UserBanToggle(string userId);
    }
}
