﻿using System.Collections.Generic;
using System.Threading.Tasks;
using XoTT_Auction.DAL.Models;

namespace XoTT_Auction.BLL.Interfaces
{
    /// <summary>
    /// Category CRUD operations
    /// </summary>
    public interface ICategoriesService
    {
        /// <summary>
        /// Create new category async
        /// </summary>
        /// <param name="item"></param>
        /// <returns>new category</returns>
        Task<Category> CreateAsync(Category item);
        /// <summary>
        /// Get all categories async
        /// </summary>
        /// <returns>all categories</returns>
        Task<List<Category>> GetAllAsync();
        /// <summary>
        /// Get all categories sync
        /// </summary>
        /// <returns>all categories</returns>
        List<Category> GetAll();
        /// <summary>
        /// Get all categories filtered by search and pagination
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        List<Category> FilterCategories(string searchTerm, int pageNo, int pageSize);
        /// <summary>
        /// Get category by category id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Category</returns>
        Category GetById(int id);
        /// <summary>
        /// Count all categories
        /// </summary>
        /// <param name="search"></param>
        /// <returns>int</returns>
        int GetCategoriesCount(string search);
        /// <summary>
        /// Update existing category
        /// </summary>
        /// <param name="item"></param>
        /// <returns>bool</returns>
        Task<bool> UpdateAsync(Category item);
        /// <summary>
        /// Delete existing category
        /// </summary>
        /// <param name="id"></param>
        /// <returns>bool</returns>
        Task<bool> DeleteAsync(int id);
    }
}
