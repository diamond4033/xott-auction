﻿using System.Threading.Tasks;
using XoTT_Auction.DAL.Models;

namespace XoTT_Auction.BLL.Interfaces
{
    /// <summary>
    /// Service for saving new picture
    /// </summary>
    public interface IPicturesService
    {
        /// <summary>
        /// Save new picture async
        /// </summary>
        /// <param name="item"></param>
        /// <returns>int</returns>
        Task<int> SavePictureAsync(Picture item);
    }
}
