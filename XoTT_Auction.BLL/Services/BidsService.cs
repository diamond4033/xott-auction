﻿using System.Threading.Tasks;
using XoTT_Auction.BLL.Interfaces;
using XoTT_Auction.DAL.Interfaces;
using XoTT_Auction.DAL.Models;

namespace XoTT_Auction.BLL.Services
{
    /// <inheritdoc cref="IBidsService"/>
    public class BidsService : IBidsService
    {
        /// <summary>
        /// bids repository variable
        /// </summary>
        private readonly IBidsRepository _bidsRepository;
        /// <summary>
        /// constructor with bids repository DI
        /// </summary>
        /// <param name="bidsRepository"></param>
        public BidsService(IBidsRepository bidsRepository)
        {
            _bidsRepository = bidsRepository;
        }
        public async Task<bool> CreateAsync(Bid item)
        {
            return await _bidsRepository.CreateAsync(item);
        }

        public int GetAllCount()
        {
            return _bidsRepository.GetAllCount();
        }
    }
}
