﻿using XoTT_Auction.DAL.Managers;
using XoTT_Auction.DAL.Models;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System;
using XoTT_Auction.BLL.Interfaces;

namespace XoTT_Auction.BLL.Services
{
    /// <inheritdoc cref="IUsersService"/>
    public class UsersService : IUsersService
    {
        /// <summary>
        /// user manager variable
        /// </summary>
        private readonly AuctionUserManager _auctionUserManager;
        /// <summary>
        /// constructor with user manager with custom password validator
        /// </summary>
        /// <param name="auctionUserManager"></param>
        public UsersService(AuctionUserManager auctionUserManager) 
        {
            _auctionUserManager = auctionUserManager;
            _auctionUserManager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 5,
                RequireNonLetterOrDigit = false,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = false
            };
        }

        public int GetUsersCount(string search)
        {
            var users = _auctionUserManager.Users.ToList();

            foreach (var user in users.ToList())
            {
                if (_auctionUserManager.IsInRole(user.Id, "Admin"))
                    users.Remove(user);
            }

            return !string.IsNullOrEmpty(search) 
                ? users.Count(u => u.FirstName.ToLower().Contains(search.ToLower()) ||
                            u.LastName.ToLower().Contains(search.ToLower()))
                : users.Count;
        }

        public async Task<IdentityResult> UserBanToggle(string userId)
        {
            var user = _auctionUserManager.FindById(userId);
            var isBanned = user.LockoutEndDateUtc >= DateTimeOffset.UtcNow;
            if (isBanned)
            {
                return await _auctionUserManager.SetLockoutEndDateAsync(userId, DateTimeOffset.MinValue);
            }
            else
            {
                return await _auctionUserManager.SetLockoutEndDateAsync(userId, DateTimeOffset.MaxValue);
            }
            
        }

        public User GetUserById(string id)
        {
            return _auctionUserManager.FindById(id);
        }

        public List<User> FilterUsers(string searchTerm, int pageNo, int pageSize)
        {
            var users = _auctionUserManager.Users.ToList();

            if (!string.IsNullOrEmpty(searchTerm))
            {
                users = users.Where(u => u.FirstName.ToLower().Contains(searchTerm.ToLower()) ||
                                         u.LastName.ToLower().Contains(searchTerm.ToLower()))
                    .ToList();
            }

            foreach (var user in users.ToList())
            {
                if (_auctionUserManager.IsInRole(user.Id, "Admin"))
                    users.Remove(user);
            }

            var skipCount = (pageNo - 1) * pageSize;

            return users.OrderByDescending(u => u.FirstName)
                .ThenBy(u => u.LastName)
                .Skip(skipCount)
                .Take(pageSize)
                .ToList();
        }

    }
}
