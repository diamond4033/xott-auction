﻿using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Threading.Tasks;
using XoTT_Auction.BLL.Dto;
using XoTT_Auction.BLL.Interfaces;
using XoTT_Auction.DAL.Managers;
using XoTT_Auction.DAL.Models;

namespace XoTT_Auction.BLL.Services
{
    /// <inheritdoc cref="IAccountsService"/>
    public class AccountsService : IAccountsService
    {
        /// <summary>
        /// user manager variable
        /// </summary>
        private readonly AuctionUserManager _auctionUserManager;
        /// <summary>
        /// constructor with user manager with custom password validator
        /// </summary>
        /// <param name="auctionUserManager"></param>
        public AccountsService(AuctionUserManager auctionUserManager)
        {
            _auctionUserManager = auctionUserManager;
            _auctionUserManager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 5,
                RequireNonLetterOrDigit = false,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = false
            };
        }
        public async Task<ClaimsIdentity> Authenticate(LoginInfoDto loginInfo)
        {
            User user = await _auctionUserManager.FindAsync(loginInfo.Username,
                loginInfo.Password);
            ClaimsIdentity claim = null;
            if (user != null)
                claim = await _auctionUserManager.CreateIdentityAsync(user,
                    DefaultAuthenticationTypes.ApplicationCookie);
            return claim;
        }
        public async Task<IdentityResult> RegisterNewUser(RegistrationInfoDto registrationInfo)
        {
            var user = new User
            {
                UserName = registrationInfo.Username,
                Email = registrationInfo.Email,
                FirstName = registrationInfo.FirstName,
                LastName = registrationInfo.LastName
            };
            IdentityResult result = await _auctionUserManager.CreateAsync(user,
                registrationInfo.Password);
            if (result.Succeeded)
            {
                user = _auctionUserManager.FindByName(user.UserName);
                result = await _auctionUserManager.AddToRoleAsync(user.Id, "User");
            }

            return result;
        }
    }
}
