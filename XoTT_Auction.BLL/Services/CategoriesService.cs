﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XoTT_Auction.BLL.Interfaces;
using XoTT_Auction.DAL.Interfaces;
using XoTT_Auction.DAL.Models;

namespace XoTT_Auction.BLL.Services
{
    /// <inheritdoc cref="ICategoriesService"/>
    public class CategoriesService : ICategoriesService
    {
        /// <summary>
        /// categories repository variable
        /// </summary>
        private readonly ICategoriesRepository _categoriesRepository;
        /// <summary>
        /// constructor with categories repository DI
        /// </summary>
        /// <param name="categoriesRepository"></param>
        public CategoriesService(ICategoriesRepository categoriesRepository)
        {
            _categoriesRepository = categoriesRepository;
        }
        public async Task<Category> CreateAsync(Category item)
        {
            return await _categoriesRepository.CreateAsync(item);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await _categoriesRepository.DeleteAsync(id);
        }

        public async Task<List<Category>> GetAllAsync()
        {
            return await _categoriesRepository.GetAllAsync();
        }

        public List<Category> GetAll()
        {
            return _categoriesRepository.GetAll();
        }

        public List<Category> FilterCategories(string searchTerm, int pageNo, int pageSize)
        {
            var allCategories = _categoriesRepository.GetAll();

            var categories = allCategories.AsQueryable();

            if (!string.IsNullOrEmpty(searchTerm))
            {
                categories = categories.Where(c => c.Name.ToLower().Contains(searchTerm.ToLower()));
            }

            var skipCount = (pageNo - 1) * pageSize;

            return categories.OrderByDescending(c => c.Id).Skip(skipCount).Take(pageSize).ToList();
        }

        public int GetSearchedCategoriesCount(string search)
        {
            var allCategories = _categoriesRepository.GetAll();

            var categories = allCategories.AsQueryable();

            if (!string.IsNullOrEmpty(search))
            {
                categories = categories.Where(c => c.Name.ToLower().Contains(search.ToLower()));
            }

            return categories.Count();
        }

        public Category GetById(int id)
        {
            return _categoriesRepository.GetById(id);
        }

        public int GetCategoriesCount(string search)
        {
            return _categoriesRepository.GetCategoriesCount(search);
        }

        public async Task<bool> UpdateAsync(Category item)
        {
            return await _categoriesRepository.UpdateAsync(item);
        }
    }
}
