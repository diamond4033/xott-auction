﻿using XoTT_Auction.DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using XoTT_Auction.BLL.Interfaces;
using XoTT_Auction.DAL.Interfaces;

namespace XoTT_Auction.BLL.Services
{
    /// <inheritdoc cref="ILotsService"/>
    public class LotsService : ILotsService
    {
        /// <summary>
        /// lots repository variable
        /// </summary>
        private readonly ILotsRepository _lotsRepository;
        /// <summary>
        /// constructor with lots repository DI
        /// </summary>
        /// <param name="lotsRepository"></param>
        public LotsService(ILotsRepository lotsRepository) 
        {
            _lotsRepository = lotsRepository;
        }

        public async Task<Lot> CreateLotAsync(Lot item)
        {
            return await _lotsRepository.CreateAsync(item);
        }

        public async Task<bool> DeleteLotAsync(int id)
        {
            return await _lotsRepository.DeleteAsync(id);
        }

        public async Task<List<Lot>> GetAllLotsAsync()
        {
            return await _lotsRepository.GetAllAsync();
        }

        public List<Lot> GetAllLots()
        {
            return _lotsRepository.GetAll();
        }

        public async Task<Lot> GetLotByIdAsync(int id)
        {
            return await _lotsRepository.GetByIdAsync(id);
        }

        public async Task<bool> UpdateLotAsync(Lot item)
        {
            return await _lotsRepository.UpdateAsync(item);
        }

        public List<Lot> FilterLots(int? categoryId, int? pageNo, int pageSize, string searchTerm, string userId)
        {
            var allLots = _lotsRepository.GetAll();

            var lots = allLots.AsQueryable();

            if (!string.IsNullOrEmpty(userId))
            {
                lots = lots.Where(l => l.UserId == userId);
            }

            if (categoryId.HasValue && categoryId.Value > 0)
            {
                lots = lots.Where(x => x.CategoryId == categoryId.Value);
            }

            if (!string.IsNullOrEmpty(searchTerm))
            {
                searchTerm = searchTerm.ToLower();
                lots = lots.Where(x => x.Title.ToLower().Contains(searchTerm));
            }

            pageNo = pageNo ?? 1;

            var skipCount = (pageNo.Value - 1) * pageSize;

            return lots.OrderByDescending(l => l.Id).Skip(skipCount).Take(pageSize).ToList();
        }

        public int GetAllLotsCount()
        {
            return GetAllLots().Count;
        }

        public int GetFilteredLotsCount(int? categoryId, string searchTerm)
        {
            var lots = GetAllLots().AsQueryable();

            if (categoryId.HasValue && categoryId.Value > 0)
            {
                lots = lots.Where(x => x.CategoryId == categoryId.Value);
            }

            if (!string.IsNullOrEmpty(searchTerm))
            {
                lots = lots.Where(x => x.Title.ToLower().Contains(searchTerm.ToLower()));
            }

            return lots.Count();
        }

        public Lot GetLotById(int id)
        {
            return _lotsRepository.GetById(id);
        }
    }
}
