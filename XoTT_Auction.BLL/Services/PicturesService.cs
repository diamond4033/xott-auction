﻿using System.Threading.Tasks;
using XoTT_Auction.BLL.Interfaces;
using XoTT_Auction.DAL.Interfaces;
using XoTT_Auction.DAL.Models;

namespace XoTT_Auction.BLL.Services
{
    /// <inheritdoc cref="IPicturesService"/>
    public class PicturesService : IPicturesService
    {
        /// <summary>
        /// pictures repository variable
        /// </summary>
        private readonly IPicturesRepository _pictureRepository;
        /// <summary>
        /// constructor with pictures repository DI
        /// </summary>
        /// <param name="pictureRepository"></param>
        public PicturesService(IPicturesRepository pictureRepository)
        {
            _pictureRepository = pictureRepository;
        }

        public async Task<int> SavePictureAsync(Picture item)
        {
            return await _pictureRepository.CreateAsync(item);
        }
    }
}
