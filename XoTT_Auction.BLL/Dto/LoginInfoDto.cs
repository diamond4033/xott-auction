﻿namespace XoTT_Auction.BLL.Dto
{
    /// <summary>
    /// Login DTO class
    /// </summary>
    public class LoginInfoDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
